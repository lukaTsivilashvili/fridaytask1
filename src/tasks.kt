fun main() {

}

//task1

fun numCount(numArray: List<Int>): Int {
    var diffElements = mutableSetOf<Int>()

    for (element in numArray) {
        diffElements.add(element)
    }
    var count = 0

    diffElements.forEach {
        count += 1
    }
    return count
}

//task2
fun numIntersection(numArray1: List<Int>, numArray2: List<Int>): Set<Int> {
    val finalSet = mutableSetOf<Int>()

    numArray1.forEach {
        if (numArray2.contains(it)) {
            finalSet.add(it)
        }
    }
    return finalSet
}

//task3

fun numUnion(numArray1: List<Int>, numArray2: List<Int>): Set<Int> {
    val finalSet = mutableSetOf<Int>()

    for (i in numArray1) {
        finalSet.add(i)
    }
    for (j in numArray2) {
        finalSet.add(j)
    }
    return finalSet
}

//task4

fun numAvgArray(numArray: List<Int>): List<Int> {
    var arrayAvg = 0
    var sum = 0

    val lessNumsArray = mutableListOf<Int>()

    numArray.forEach {
        sum += it
    }
    arrayAvg = sum / numArray.size

    for (i in 0 until arrayAvg) {
        lessNumsArray.add(i)
    }

    return lessNumsArray

}

fun secondMinMax(numArray: List<Int>): Map<String, Int?> {

    val sortedArray = numArray.sorted()
    val sortedNumSet = sortedArray.toSet().sorted()


    var secondMin = sortedNumSet[1]
    var secondMax = sortedNumSet[sortedNumSet.size - 2]

    val resMap = mutableMapOf<String, Int>("მეორე უმცირესი " to secondMin, "მეორე მაქსიმალური " to secondMax)

    return resMap
}